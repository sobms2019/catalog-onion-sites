import requests
import os
from bs4 import BeautifulSoup
import time
url = 'https://best.aliexpress.ru/'
proxies = {'htpps': '103.78.181.149'}

used = set()
# def get_result(url):
#     rs = requests.session()
#     rs.proxies['http'] = os.getenv("proxy", "socks5h://localhost:9150")
#     rs.proxies['https'] = os.getenv("proxy", "socks5h://localhost:9150")
#     rs.headers['User-Agent'] = 'Mozilla/5.0 (Windows NT 6.1; rv:60.0) Gecko/20100101 Firefox/60.0'
#     rs.headers['Accept-Language'] = 'Accept-Language: en-US,en;q=0.5'
#     result = rs.get(url, timeout=1000)
#     rs.close()
#     return result

def dfs(link):
    global used, url
    print(link)
    used.add(link)
    #time.sleep(2)
    try:
        html_page = requests.get(url='https://best.aliexpress.ru/', proxies=proxies)
    except:
        return
    parse_page = BeautifulSoup(html_page.text, 'lxml')
    for html_element in parse_page.findAll('a'):
        if str(html_element.get('href'))[:4] == 'http':
            continue
        new_link = url + str(html_element.get('href'))

        if new_link not in used:
            dfs(new_link)


dfs(url)
print(used)
