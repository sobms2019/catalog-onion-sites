import nltk
import numpy
from nltk import  pos_tag, ne_chunk
from nltk import conlltags2tree, tree2conlltags
import re
import pprint
from nltk.corpus import conll2000

f = open('C:\data.txt', 'r')
f2 = open('C:\data_conll.txt','w',encoding='utf-8')
str_ex = "A government can limit access to the internet by ordering internet service providers (ISPs) to limit access to their subscribers. In the first instance, this is likely to be a block on commonly used social media sites. As a more extreme measure, the authorities can order service providers to block all internet access. Ivory Coast, DR Congo, Chad, Cameroon, Sudan, Ethiopia, Mali, Nigeria and Sierra Leone restricted access to the internet last year."
def ie_preprocess(document):                                   #define of part of speech, the devision inti sentences and words
   sentences = nltk.sent_tokenize(document)
   sentences = [nltk.word_tokenize(sent) for sent in sentences]
   sentences = [nltk.pos_tag(sent) for sent in sentences]
   # print(sentences)
   return sentences

train_sents = conll2000.chunked_sents('train.txt', chunk_types=['NP'])
test_sents = conll2000.chunked_sents('test.txt', chunk_types=['NP'])
def npchunk_features(sentence, i, history):
    word, pos = sentence[i]
    if i == 0:
        prevword, prevpos = "<START>", "<START>"
    else:
        prevword, prevpos = sentence[i - 1]
        return {"pos": pos, "prevpos": prevpos}

class UnigramChunker(nltk.ChunkParserI):
    def __init__(self, train_sents): # [_code-unigram-chunker-constructor]
        train_data = [[(t,c) for w,t,c in nltk.chunk.tree2conlltags(sent)]
                      for sent in train_sents]
        self.tagger = nltk.UnigramTagger(train_data) # [_code-unigram-chunker-buildit]

    def parse(self, sentence): # [_code-unigram-chunker-parse]
        pos_tags = [pos for (word,pos) in sentence]
        tagged_pos_tags = self.tagger.tag(pos_tags)
        chunktags = [chunktag for (pos, chunktag) in tagged_pos_tags]
        conlltags = [(word, pos, chunktag) for ((word,pos),chunktag)
                     in zip(sentence, chunktags)]
        return nltk.chunk.conlltags2tree(conlltags)

test_sents = conll2000.chunked_sents('test.txt', chunk_types=['NP'])
train_sents = conll2000.chunked_sents('train.txt', chunk_types=['NP'])
unigram_chunker = UnigramChunker(train_sents)
unigram_chunker.evaluate(test_sents)

tagging_sentenses = ie_preprocess(str_ex)

def write_in_table(iob_tag):
    for tupe in iob_tag:
        f2.write(str(tupe[0]) + ' ' + str(tupe[1]) + ' ' + str(tupe[2]) + '\n')

for tagging_sent in tagging_sentenses:
   leaf = unigram_chunker.parse(tagging_sent);
   print(tree2conlltags(leaf))
   write_in_table(tree2conlltags(leaf))

